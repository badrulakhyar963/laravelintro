<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@Home');
Route::get('/Register', 'AuthController@Register');

Route::get('/Welcome', 'AuthController@kirim');

// CRUD cast
// create
Route::get('/cast/create', 'castController@create');
Route::post('/cast', 'castController@store');

// read
Route::get('/cast/create', 'castController@index');
Route::get('/cast/{cast_id}', 'castController@show');

// update
Route::get('/cast/{cast_id}/edit', 'castController@edit');
Route::put('/cast/{cast_id}', 'castController@update');

//delete
Route::delete('/cast/{cast_id}', 'castController@udestroy');


