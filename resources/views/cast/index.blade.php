<a  href="/cast/create" class="btn btn-primnary mb-3">tambah cast</a>

<table class="table">
  <thead class="thead-dark">
    <tr>
      <th scope="col">#</th>
      <th scope="col">nama</th>
      <th scope="col">umur</th>
      <th scope="col">bio</th>
      <th scope="col">action</th>
    </tr>
  </thead>
  <tbody>
    @forelse (@cast as $key=> $item)
    <tr>
        <td>{{$key + 1}}</td>
        <td>{{$item->nama}}</td>
        <td>{{$item->umur}}</td>
        <td>{{$item->bio}}</td>
        <td>
            
            <form action="/cast/{{$item->id}}" method="POST">
         @csrf
         @method('delete')   
         <a  href="/cast/{{$item->id}}}">Detail</a>
            <a  href="/cast/{cast_id}/edit">edit</a>
         <input type="submit" class="btn btn-primary btn-sm" value="delete">
        </form>

        </td> 
    </tr>
    @empty
    <h1>Data tidak ada</h1>
    @endforelse
  </tbody>
  
</table>